import socket
import threading

bind_ip = "0.0.0.0"
bind_port = 8000

server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
server.bind((bind_ip,bind_port))

# listen(backlog), in new version of python backlog should be optional
# backlog it can be 0 - 5
# indicates how many connection can be queued since TCP need to accept the connection
# more info: https://stackoverflow.com/questions/36594400/what-is-backlog-in-tcp-connections
server.listen()

print ("[*] Listening on %s:%d" % (bind_ip,bind_port))

# This is client handling thread
def handle_client (client_socket):

    # Print out what the client sends
    # In case I forgot, .recv is for TCP .recvfrom is for UDP
    request = client_socket.recv(1024)
    print ("[*] Received: %s" % (request))
    # Send back a packet
    client_socket.send("ACK!".encode())
    client_socket.close()

while True:
    # Receive client socket into the client variable
    # and remote connection details into the addr variable
    client,addr = server.accept()
    print ("[*] Accepted connection from: %s:%d" % (addr[0],addr[1]))
    # Spin up our client thread to handle incoming data
    # Create thread handling and point it to handle_client function
    # and pass client as parameter
    client_handler = threading.Thread(target=handle_client,args=(client,))
    client_handler.start()

