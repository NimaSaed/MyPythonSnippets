import argparse


parser = argparse.ArgumentParser(
        description="This is my arg parser example"
        )

parser.add_argument('-n', action='store',
                    dest='name',
                    help='store name'
                    )
parser.add_argument('-a',action='store',
                    dest='age',
                    help='store age',
                    type=int
                    )

result = parser.parse_args()
print(result)
print("Hi, my name is %s and I am %d years old" % (result.name,result.age))
