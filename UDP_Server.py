import socket
import threading

bind_ip = "127.0.0.1"
bind_port = 8000

server = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
server.bind((bind_ip, bind_port))

while True:
    # Incase I forgot, .recv is for TCP .recvfrom is for UDP
    data,addr = server.recvfrom(4096)
    print (data)
    print (addr)
    server.sendto("ACT".encode(),(addr))
