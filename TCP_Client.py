import socket

target_host = "0.0.0.0"
target_port = 8000

# Create a socket object
# AF_INET means we are going to use standard IPv4 or hostname
# SOCK_STREAM means this will be TCP client
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the client 
client.connect((target_host,target_port))

# Create some date, encode it and send it 
data=("GET / HTTP/1.1\r\nHost: google.com\r\n\r\n").encode()
client.send(data)

# Receive the data
response = client.recv(4096)
print (response)
